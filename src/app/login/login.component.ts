import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../login.service';
import { AuthService } from '../auth.service';
import { LocalStorageService } from '../localStorage.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  public email = 'add@email.here';
  public password = '*******';

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private authService: AuthService,
    private localStorageService: LocalStorageService,
    private http: HttpClient,
  ) {}
  loggedIn = false;
  brewerie$ = this.authService;
  formGroup: FormGroup = this.fb.group({
    email: ['email', [Validators.required, Validators.email]],
    password: ['password', [Validators.required, Validators.pattern('^(.{0,7}|[^0-9]*|[^A-Z]*|[a-zA-Z0-9]*)$')]],
  });
  public loginButtonClicked(data: { password: string; email: string }) {
    this.loginService
      .login(data.email, data.password)
      .pipe(
        map((value) => {
          console.log('value', value);
          this.localStorageService.setItem('jwt-token', value.token);
          console.log('value-token', value.token);
          return value;
        }),
      )
      .subscribe();
  }

  login(email: string, password: string): Observable<{}> {
    return this.http.post(
      'http://localhost:8080/login',
      {
        email,
        password,
      },
      { responseType: 'json' },
    );
  }
}
