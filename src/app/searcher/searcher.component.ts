import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Brewery } from '../interfaces/brewerie-response';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-searcher',
  templateUrl: './searcher.component.html',
  styleUrls: ['./searcher.component.scss'],
})
export class SearcherComponent {
  constructor(private http: HttpClient) {}
  fetchbreweries(): Observable<Brewery> {
    return this.http.get('http://localhost:4200/breweries') as Observable<Brewery>;
  }
}
