import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private httpClient: HttpClient) {}

  public login(email: string, password: string) {
    // tslint:disable-next-line: no-console
    console.trace();
    return this.httpClient.post<{ token: string }>('http://localhost:8080/login', { email, password });
  }
}
