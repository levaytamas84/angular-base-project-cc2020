import { Component, OnInit } from '@angular/core';
/* import { FormBuilder, FormGroup, Validators } from '@angular/forms'; */
import { AuthService } from './auth.service';
import { Brewery } from './interfaces/brewerie-response';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'JWTAuth';
  brewerie: Brewery;
  constructor(private authService: AuthService /* private fb: FormBuilder */) {}
  brewerie$ = this.authService;

  /*   formGroup: FormGroup = this.fb.group({
    email: ['email', [Validators.required, Validators.email]],
    password: ['password', [Validators.required, Validators.pattern('^(.{0,7}|[^0-9]*|[^A-Z]*|[a-zA-Z0-9]*)$')]],
  }); */
  ngOnInit() {}

  /*   login() {
    console.log(this.formGroup.value);
  } */
}
