//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LocalStorageService } from './localstorage.service';
@Injectable({
  providedIn: 'root',
})
export class AuthService implements CanActivate {
  constructor(private localStorageService: LocalStorageService) {}
  // tslint:disable-next-line: variable-name
  public canActivate(_route: ActivatedRouteSnapshot): boolean {
    const jwtHelper = new JwtHelperService();
    const jwtToken = this.localStorageService.getItem('jwt-token');
    if (jwtToken) {
      return jwtHelper.isTokenExpired(jwtToken);
    }
    return false;
  }
}
